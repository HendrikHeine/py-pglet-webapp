from pglet import Stack
from pglet import Textbox
from pglet import Button
from pglet import Checkbox
from pglet import Event
from pglet import Page


class Login:
    def __init__(self, page:Page) -> None:
        self.__page = page

        self.view_stack = Stack()
        self.login_button = Button(
            text="Login",
            on_click=self.on_click_login,
            padding=1
        )
        self.register_button = Button(
            text="Register", 
            on_click=self.on_click_register,
            padding=2
        )

        self.keep_logged_in_checkbox = Checkbox(
            label="Keep logged in",
            value=False,
            padding=3,
            on_change=self.on_change_keep_logged_in_checkbox
        )

        self.email_textbox = Textbox(
            placeholder="eMail",
            required=True
        )

        self.password_textbox = Textbox(
            placeholder="Password",
            password=True,
            required=True
        )

        self.textbox_stack = Stack(
            max_width="100%",
            vertical_align="center",
            controls=[
                self.email_textbox,
                self.password_textbox
            ]
        )

        self.button_stack = Stack(
            width="100%",
            vertical_align="center",
            horizontal=True,
            controls=[
                self.login_button,
                self.register_button,
                self.keep_logged_in_checkbox
            ]
        )
        
        self.view = Stack(
            width="100%",
            vertical_align="center",
            controls=[
                self.textbox_stack,
                self.button_stack
            ]
        )

    def on_click_login(self, e:Event):
        pass

    def on_click_register(self, e:Event):
        pass

    def on_change_keep_logged_in_checkbox(self, e:Event):
        self.keep_logged_in_checkbox.focused = False
        self.__page.update()
        
