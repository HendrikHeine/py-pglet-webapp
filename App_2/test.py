import pglet
from pglet import SplitStack, Stack, Text, Button
menu = Stack(height="100%", width="10%", min_width="200", controls=[Text("Column A")], border_color="black", border_style="solid", visible=False)
def onClick(e):
    if menu.visible:
        menu.visible = False
    else:
        menu.visible = True
    page.update()
button = Button(text="Menu", on_click=onClick)

screen = Stack(height="100%", width="100%", controls=[Text("Column B"), button], border_color="black", border_style="solid")

def split_resize(e):
    for c in e.control.controls:
        print("size", c.width if e.control.horizontal else c.height)



page = pglet.page("split")
page.horizontal_align = "stretch"
page.vertical_fill = True
page.add(SplitStack(
    height="100%",
    horizontal=True,
    gutter_size=10, 
    controls=[
        menu,
        screen
    ],
))

input()