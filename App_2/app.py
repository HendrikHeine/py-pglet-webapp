from pglet import Text
from pglet import Stack
from pglet import Textbox
from pglet import Button
from pglet import Tabs
from pglet import Tab
from pglet import Toggle
from pglet import Event
from pglet import Page

from settings import *
from views.login import Login
from views.menu import Menu

class App:
    def __init__(self, page:Page) -> None:
        self.__page = page
        if self.__page.theme == "dark": self.__default_theme_toggle_value = False
        if self.__page.theme == "light": self.__default_theme_toggle_value = True

        self.view_stack = Stack()
        self.login_view = Login(page)
        self.menu = Menu(page)
        self.caption = Text(
            value=APP_CAPTION, 
            size="xxLarge",
            bold=True,
            width="100%", 
            padding=1
        )

        self.theme_toggle = Toggle(
            #label='Theme', 
            padding=2,
            value=self.__default_theme_toggle_value,
            on_change=self.on_change_theme_toggle,
            on_text="Light",
            off_text="Dark"
        )

        self.view = Stack(
            width="70%",    
            controls=[
                self.view_stack,
                self.menu.view,
                self.login_view.view,
            ]
        )

        self.header_stack = Stack(
            width="100%",
            horizontal_align="end",
            horizontal=True,
            controls=[
                self.caption,
                self.theme_toggle
            ]
        )

        self.head = Stack(
            width="100%",
            horizontal_align="center",
            controls=[
                self.header_stack
            ]
        )
        

    def on_change_theme_toggle(self, e:Event):
        if self.theme_toggle.value:
            self.__page.theme = "light"
            self.__page.update()
        else:
            self.__page.theme = "dark"
            self.__page.update()