from pglet import Stack
from pglet import Textbox
from pglet import Button
from pglet import Checkbox


class Task:
    def __init__(self, app, name):
        self.app = app
        self.display_task = Checkbox(
            value=False, label=name, on_change=self.status_changed
        )
        self.edit_name = Textbox(width="100%")

        self.display_view = Stack(
            horizontal=True,
            horizontal_align="space-between",
            vertical_align="center",
            controls=[
                self.display_task,
                Stack(
                    horizontal=True,
                    gap="0",
                    controls=[
                        Button(
                            icon="Edit", title="Edit todo", on_click=self.edit_clicked
                        ),
                        Button(
                            icon="Delete",
                            title="Delete todo",
                            on_click=self.delete_clicked,
                        ),
                    ],
                ),
            ],
        )

        self.edit_view = Stack(
            visible=False,
            horizontal=True,
            horizontal_align="space-between",
            vertical_align="center",
            controls=[self.edit_name, Button(text="Save", on_click=self.save_clicked)],
        )
        self.view = Stack(controls=[self.display_view, self.edit_view])

    def edit_clicked(self, e):
        self.edit_name.value = self.display_task.label
        self.display_view.visible = False
        self.edit_view.visible = True
        self.view.update()

    def save_clicked(self, e):
        self.display_task.label = self.edit_name.value
        self.display_view.visible = True
        self.edit_view.visible = False
        self.view.update()

    def delete_clicked(self, e):
        self.app.delete_task(self)

    def status_changed(self, e):
        self.app.update()
