import pglet

from todo import *


def main(page:pglet.Page):
    page.title = "ToDo App"
    page.horizontal_align = "center"
    page.height = "100%"
    page.theme = "dark"
    page.update()

    # create application instance
    app = TodoApp()

    # add application's root control to the page
    page.add(app.view)


pglet.app("todo-app", target=main)