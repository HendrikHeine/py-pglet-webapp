
from pglet import Text
from pglet import Stack
from pglet import Textbox
from pglet import Button
from pglet import Tabs
from pglet import Tab

from task import Task

TEXT_COLOR = "white"

class TodoApp:
    def __init__(self):
        self.tasks = []
        self.new_task = Textbox(placeholder="Your ToDo:", width="100%")
        self.tasks_view = Stack()

        self.filter = Tabs( 
            value="all",
            on_change=self.tabs_changed,
            tabs=[
                Tab(text="all"), 
                Tab(text="active"), 
                Tab(text="completed")
            ],
        )

        self.view = Stack(
            width="70%",
            controls=[
                Text(value="Todos", size="large", align="center", color=TEXT_COLOR),
                Stack(
                    horizontal=True,
                    on_submit=self.add_clicked,
                    controls=[
                        self.new_task,
                        Button(primary=True, text="Add", on_click=self.add_clicked),
                    ],
                ),
                Stack(gap=25, controls=[self.filter, self.tasks_view]),
            ],
        )

    def update(self):
        status = self.filter.value
        for task in self.tasks:
            task.view.visible = (
                status == "all"
                or (status == "active" and task.display_task.value == False)
                or (status == "completed" and task.display_task.value)
            )
        self.view.update()

    def add_clicked(self, e):
        task = Task(self, self.new_task.value)
        self.tasks.append(task)
        self.tasks_view.controls.append(task.view)
        self.new_task.value = ""
        self.update()

    def delete_task(self, task):
        self.tasks.remove(task)
        self.tasks_view.controls.remove(task.view)
        self.update()

    def tabs_changed(self, e):
        self.update()
